package com.example.gulevskiy.kinopoisk;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CitiesActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cities);

        mRecyclerView = (RecyclerView) findViewById(R.id.card_cities_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        setupAdapter();
    }

    private void setupAdapter() {
        List<City> cities = CitiesLab.get().getCities();
        CitiesAdapter citiesAdapter = new CitiesAdapter(cities);
        mRecyclerView.setAdapter(citiesAdapter);
    }

    private class CitiesAdapter extends RecyclerView.Adapter<CitiesHolder> {
        private List<City> mCities;

        public CitiesAdapter(List<City> cities) {
            mCities = cities;
        }

        @Override
        public CitiesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater
                    .from(parent.getContext()).inflate(R.layout.cities_card_view, parent, false);
            return new CitiesHolder(view);
        }

        @Override
        public void onBindViewHolder(CitiesHolder holder, int position) {
            City city = mCities.get(position);
            holder.bindCityItem(city);
        }

        @Override
        public int getItemCount() {
            return mCities.size();
        }
    }

    private class CitiesHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener,
            View.OnCreateContextMenuListener,
            MenuItem.OnMenuItemClickListener {
        private static final String GET_CINEMAS = "Показать кинотеатры";
        private TextView mTitleTextView;
        private City mCity;

        public CitiesHolder(View itemView) {
            super(itemView);
            CardView mCardView = (CardView) itemView;
            mCardView.setOnClickListener(this);
            mCardView.setOnCreateContextMenuListener(this);
            mTitleTextView = (TextView) itemView.findViewById(R.id.text_view);
        }

        private void bindCityItem(City item) {
            mTitleTextView.setText(item.toString());
            mCity = item;
        }

        @Override
        public void onClick(View v) {
            // Показываем фильмы в прокате
            Intent intent = FilmListActivity
                    .newIntent(getApplicationContext(), mCity.getID());
            startActivity(intent);
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu,
                                        View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            if (v.getId() == R.id.card && mCity != null) {
                MenuItem menuItem = menu.add(0, v.getId(), 0, GET_CINEMAS);
                menuItem.setOnMenuItemClickListener(this);
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (item.getTitle() == GET_CINEMAS) {
                // Показываем список кинотеатров в городе
                Intent intent = CinemasActivity
                        .newIntent(getApplicationContext(), mCity.getID());
                startActivity(intent);
            }
            return false;
        }
    }
}
