package com.example.gulevskiy.kinopoisk;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private static final String EXTRA_LNG = "LNG";
    private static final String EXTRA_LAT = "LAT";
    private static final String EXTRA_CINEMA_NAME = "CINEMA_NAME";

    private GoogleMap mMap;
    private String mCinemaName;
    private LatLng mLatLng;

    public static Intent newIntent(Context packageContext,
                                   Double lng,
                                   Double lat,
                                   String cinemaName) {
        Intent intent = new Intent(packageContext, MapsActivity.class);
        intent.putExtra(EXTRA_CINEMA_NAME, cinemaName);
        intent.putExtra(EXTRA_LAT, lat);
        intent.putExtra(EXTRA_LNG, lng);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (mMap == null) {
            return;
        }

        mCinemaName = getIntent().getStringExtra(EXTRA_CINEMA_NAME);
        setTitle(mCinemaName);

        mLatLng = new LatLng(
                getIntent().getDoubleExtra(EXTRA_LAT, 0),
                getIntent().getDoubleExtra(EXTRA_LNG, 0));

        updateUI();
    }

    private void updateUI() {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mLatLng, 15.0f));
        mMap.addMarker(new MarkerOptions()
                .position(mLatLng)
                .title(mCinemaName)
                .draggable(false)
        );
    }
}
