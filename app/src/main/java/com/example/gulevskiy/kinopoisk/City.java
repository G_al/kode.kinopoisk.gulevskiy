package com.example.gulevskiy.kinopoisk;

public class City {
    private int mID;
    private String mName;

    public City(int cityID, String cityName) {
        mID = cityID;
        mName = cityName;
    }

    public int getID() {
        return mID;
    }

    @Override
    public String toString() {
        return mName;
    }
}
