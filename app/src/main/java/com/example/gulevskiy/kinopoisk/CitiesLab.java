package com.example.gulevskiy.kinopoisk;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton для хранения списка городов
 */
public class CitiesLab {
    private static CitiesLab sCitiesLab;
    private List<City> mCities;

    private CitiesLab() {
        addCities();
    }

    public static CitiesLab get() {
        if (sCitiesLab == null) {
            sCitiesLab = new CitiesLab();
        }
        return sCitiesLab;
    }

    public List<City> getCities() {
        return mCities;
    }

    public void addCities() {
        mCities = new ArrayList<>();
        mCities.add(new City(490, "Калининград"));
        mCities.add(new City(430, "Казань"));
        mCities.add(new City(1, "Москва"));
        mCities.add(new City(2, "Санкт-Петербург"));
        mCities.add(new City(5266, "Севастополь"));
    }
}
