package com.example.gulevskiy.kinopoisk;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FilmListActivity extends AppCompatActivity {
    private static final String EXTRA_CITY_ID = "city_id";
    private static final String SPINNER_TITLE = "Выбор жанра";
    private static final String SET_FILTER_TITLE = "Фильтровать по жанру";
    private static final String REMOVE_FILTER_TITLE = "Снять фильтр";
    private static final String IS_FILTERED = "IS_FILTERED";
    private static final String IS_SORTED = "IS_SORTED";
    private static final String SPINNER_POSITION = "SPINNER_POSITION";

    private RecyclerView mRecyclerView;
    private Menu mMenu;
    private Spinner mSpinner;
    private List<Film> mFilms;
    private List<Film> mUnfilteredFilms;
    private List<String> mGenres;
    private Integer mCityId;
    private Integer mSpinnerPosition;
    private boolean mIsFiltered;
    private boolean mIsSorted;

    public static Intent newIntent(Context packageContext,
                                   Integer cityId) {
        Intent intent = new Intent(packageContext, FilmListActivity.class);
        intent.putExtra(EXTRA_CITY_ID, cityId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list);

        mRecyclerView = (RecyclerView) findViewById(R.id.card_films_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mCityId = getIntent().getIntExtra(EXTRA_CITY_ID, 0);

        if (savedInstanceState != null) {
            if (savedInstanceState.getBoolean(IS_FILTERED)) {
                mIsFiltered = true;
                mSpinnerPosition = savedInstanceState.getInt(SPINNER_POSITION);
            }
            mIsSorted = savedInstanceState.getBoolean(IS_SORTED);
        }

        new FetchFilmsTask().execute(mCityId);
    }

    private void updateMenuItems() {
        MenuItem item = mMenu.findItem(R.id.menu_item_genre_filter);
        if (mIsFiltered) {
            item.setTitle(REMOVE_FILTER_TITLE);
        } else {
            item.setTitle(SET_FILTER_TITLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.mMenu = menu;
        getMenuInflater().inflate(R.menu.activity_films, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_sort_films:
                sortFilmsByRating();
                setupAdapter();
                return true;
            case R.id.menu_item_genre_filter:
                if (!mIsFiltered) {
                    mSpinner.setVisibility(View.VISIBLE);
                    mSpinner.performClick();
                    mIsFiltered = true;
                } else {
                    mSpinner.setVisibility(View.GONE);
                    mIsFiltered = false;
                    mFilms = mUnfilteredFilms;
                    setupAdapter();
                }
                updateMenuItems();
                return true;
            default:
                return false;
        }
    }

    private void sortFilmsByRating() {
        Collections.sort(mFilms, Collections.<Film>reverseOrder());
        mIsSorted = true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(IS_SORTED, mIsSorted);
        outState.putBoolean(IS_FILTERED, mIsFiltered);
        outState.putInt(SPINNER_POSITION, mSpinner.getSelectedItemPosition());
    }

    private void setupSpinner() {
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(this, R.layout.spinner_layout, mGenres);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mSpinner = (Spinner) findViewById(R.id.spinner);
        if (mSpinner != null) {
            mSpinner.setAdapter(adapter);
            mSpinner.setPrompt(SPINNER_TITLE);

            if (mIsFiltered) {
                mSpinner.setVisibility(View.VISIBLE);
                mSpinner.setSelection(mSpinnerPosition);
            }

            mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent,
                                           View view, int position, long id) {
                    filterFilmsByGenre(mSpinner.getSelectedItem().toString());
                    setupAdapter();
                    updateMenuItems();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    private void filterFilmsByGenre(String genre) {
        ArrayList<Film> filteredList = new ArrayList<>();
        for (Film film : mUnfilteredFilms) {
            if (film.getGenre().contains(genre)) {
                filteredList.add(film);
            }
        }
        mFilms = filteredList;
    }

    private void setupAdapter() {
        FilmsAdapter filmsAdapter = new FilmsAdapter(mFilms);
        mRecyclerView.setAdapter(filmsAdapter);
    }

    private class FilmsAdapter extends RecyclerView.Adapter<FilmsHolder> {
        private List<Film> mFilms;

        public FilmsAdapter(List<Film> films) {
            mFilms = films;
        }

        @Override
        public FilmsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater
                    .from(parent.getContext()).inflate(R.layout.films_card_view, parent, false);
            return new FilmsHolder(view);
        }

        @Override
        public void onBindViewHolder(FilmsHolder holder, int position) {
            Film film = mFilms.get(position);
            holder.bindFilmItem(film);
        }

        @Override
        public int getItemCount() {
            return mFilms.size();
        }
    }

    private class FilmsHolder
            extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnCreateContextMenuListener, MenuItem.OnMenuItemClickListener {
        private static final String GET_SEANCES = "Показать сеансы";

        private TextView mTitleTextView;
        private TextView mGenreTextView;
        private TextView mRatingTextView;
        private Film mFilm;

        public FilmsHolder(View itemView) {
            super(itemView);

            CardView mCardView = (CardView) itemView;
            mCardView.setOnClickListener(this);
            mCardView.setOnCreateContextMenuListener(this);

            mTitleTextView = (TextView) itemView.findViewById(R.id.title_text_view);
            mGenreTextView = (TextView) itemView.findViewById(R.id.genre_text_view);
            mRatingTextView = (TextView) itemView.findViewById(R.id.rating_text_view);
        }

        private void bindFilmItem(Film item) {
            mTitleTextView.setText(item.toString());
            mGenreTextView.setText(item.getGenre());
            mRatingTextView.setText(item.getRating());
            mFilm = item;
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu,
                                        View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            if (v.getId() == R.id.card && mFilm != null) {
                MenuItem menuItem = menu.add(0, v.getId(), 0, GET_SEANCES);
                menuItem.setOnMenuItemClickListener(this);
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            if (item.getTitle() == GET_SEANCES) {
                showSeances();
            }
            return false;
        }

        private void showSeances() {
            String url = "https://www.kinopoisk.ru/afisha/film/" + mFilm.getId() + "/" + mCityId;
            displayWebPageByURL(url);
        }

        private void displayWebPageByURL(String uriString) {
            Uri address = Uri.parse(uriString);
            Intent intent = new Intent(Intent.ACTION_VIEW, address);
            startActivity(intent);
        }

        @Override
        public void onClick(View v) {
            // Открываем подробную информацию по выбранному фильму
            Intent intent = FilmActivity
                    .newIntent(getApplicationContext(), mFilm);
            startActivity(intent);
        }
    }

    private class FetchFilmsTask extends AsyncTask<Integer, Void, List<Film>> {
        @Override
        protected List<Film> doInBackground(Integer... params) {
            return new KinopoiskFetchr().fetchFilms(params[0]);
        }

        @Override
        protected void onPostExecute(List<Film> films) {
            mFilms = new ArrayList<>();
            for (Film film : films) {
                mFilms.add(film);
            }
            mUnfilteredFilms = mFilms;

            new FetchGenresTask().execute();

            if (mIsSorted) {
                sortFilmsByRating();
            }
            setupAdapter();
        }
    }

    private class FetchGenresTask extends AsyncTask<Void, Void, List<Genre>> {
        @Override
        protected List<Genre> doInBackground(Void... params) {
            return new KinopoiskFetchr().fetchGenres();
        }

        @Override
        protected void onPostExecute(List<Genre> genres) {
            mGenres = new ArrayList<>();
            for (Genre genre : genres) {
                mGenres.add(genre.getName());
            }
            setupSpinner();
        }
    }
}
