package com.example.gulevskiy.kinopoisk;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CinemasActivity extends AppCompatActivity {
    private static final String EXTRA_CITY_ID
            = "com.example.gulevskiy.kinopoisk.city_id";

    private RecyclerView mRecyclerView;
    private List<Cinema> mCinemas;

    public static Intent newIntent(Context packageContext,
                                   Integer cityId) {
        Intent intent = new Intent(packageContext, CinemasActivity.class);
        intent.putExtra(EXTRA_CITY_ID, cityId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinemas);

        mRecyclerView = (RecyclerView) findViewById(R.id.card_cinemas_recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        Integer cityId = getIntent().getIntExtra(EXTRA_CITY_ID, 0);
        new FetchCinemasTask().execute(cityId);
    }

    private void setupAdapter() {
        CinemasAdapter citiesAdapter = new CinemasAdapter(mCinemas);
        mRecyclerView.setAdapter(citiesAdapter);
    }

    private class CinemasAdapter extends RecyclerView.Adapter<CinemasHolder> {
        private List<Cinema> mCinemas;

        public CinemasAdapter(List<Cinema> cinemas) {
            mCinemas = cinemas;
        }

        @Override
        public CinemasHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater
                    .from(parent.getContext())
                    .inflate(R.layout.cinemas_card_view, parent, false);
            return new CinemasHolder(view);
        }

        @Override
        public void onBindViewHolder(CinemasHolder holder, int position) {
            Cinema cinema = mCinemas.get(position);
            holder.bindCinemaItem(cinema);
        }

        @Override
        public int getItemCount() {
            return mCinemas.size();
        }
    }

    private class CinemasHolder
            extends RecyclerView.ViewHolder
            implements View.OnClickListener {
        private TextView mNameTextView;
        private TextView mAddressTextView;
        private Cinema mCinema;

        public CinemasHolder(View itemView) {
            super(itemView);
            CardView mCardView = (CardView) itemView;
            mCardView.setOnClickListener(this);
            mNameTextView = (TextView) itemView.findViewById(R.id.cinema_text_view);
            mAddressTextView = (TextView) itemView.findViewById(R.id.address_text_view);
        }

        private void bindCinemaItem(Cinema item) {
            mNameTextView.setText(item.getName());
            mAddressTextView.setText(item.getAddress());
            mCinema = item;
        }

        @Override
        public void onClick(View v) {
            // Показываем кинотеатр на карте
            Intent intent = MapsActivity
                    .newIntent(getApplicationContext(),
                            mCinema.getLongitude(),
                            mCinema.getLatitude(),
                            mCinema.getName());
            startActivity(intent);
        }
    }

    private class FetchCinemasTask extends AsyncTask<Integer, Void, List<Cinema>> {
        @Override
        protected List<Cinema> doInBackground(Integer... params) {
            return new KinopoiskFetchr().fetchCinemas(params[0]);
        }

        @Override
        protected void onPostExecute(List<Cinema> cinemas) {
            mCinemas = new ArrayList<>();
            for (Cinema cinema : cinemas) {
                mCinemas.add(cinema);
            }
            setupAdapter();
        }
    }
}