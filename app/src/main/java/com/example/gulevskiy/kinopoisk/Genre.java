package com.example.gulevskiy.kinopoisk;

public class Genre {
    private String mName;

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
