package com.example.gulevskiy.kinopoisk;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class FilmActivity extends AppCompatActivity {
    private static final String EXTRA_FILM =
            "com.example.gulevskiy.kinopoisk.film";

    private TextView mNameEnTextView;
    private TextView mCountryTextView;
    private TextView mYearTextView;
    private TextView mFilmLengthTextView;
    private TextView mDescriptionTextView;
    private TextView mGenreTextView;
    private volatile Film mFilm;

    public static Intent newIntent(Context packageContext,
                                   Film film) {
        Intent intent = new Intent(packageContext, FilmActivity.class);
        intent.putExtra(EXTRA_FILM, film);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);

        mFilm = (Film) getIntent().getSerializableExtra(EXTRA_FILM);

        mNameEnTextView = (TextView) findViewById(R.id.name_en);
        mCountryTextView = (TextView) findViewById(R.id.country);
        mYearTextView = (TextView) findViewById(R.id.year);
        mFilmLengthTextView = (TextView) findViewById(R.id.film_length);
        mDescriptionTextView = (TextView) findViewById(R.id.description);
        mGenreTextView = (TextView) findViewById(R.id.genre);

        setTitle(mFilm.getNameRU());

        new FetchAndAddDetailsTask().execute(mFilm);
    }

    private void updateUI() {
        mCountryTextView.setText(String.format(getResources()
                .getString(R.string.country_title), mFilm.getCountry()));
        mYearTextView.setText(String.format(getResources()
                .getString(R.string.year_title), mFilm.getYear()));
        mFilmLengthTextView.setText(String.format(getResources()
                .getString(R.string.length_title), mFilm.getLength()));
        mDescriptionTextView.setText(mFilm.getDescription());
        mNameEnTextView.setText(String.format(getResources()
                .getString(R.string.name_en_title), mFilm.getNameEN()));
        mGenreTextView.setText(String.format(getResources()
                .getString(R.string.genre_title), mFilm.getGenre()));
    }

    private class FetchAndAddDetailsTask extends AsyncTask<Film, Void, Void> {
        @Override
        protected Void doInBackground(Film... params) {
            new KinopoiskFetchr().fetchFilmDetails(params[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            updateUI();
        }
    }
}
