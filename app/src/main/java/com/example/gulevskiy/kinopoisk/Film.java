package com.example.gulevskiy.kinopoisk;

import android.support.annotation.NonNull;

import java.io.Serializable;

public class Film implements Comparable<Film>, Serializable {
    private Integer mId;
    private String mNameRU;
    private String mGenre;
    private String mRating;
    private String mDescription;
    private String mNameEN;
    private String mCountry;
    private String mYear;
    private String mLength;

    public String getGenre() {
        return mGenre;
    }

    public void setGenre(String genre) {
        mGenre = genre;
    }

    public Integer getId() {
        return mId;
    }

    public void setId(Integer id) {
        mId = id;
    }

    public String getNameRU() {
        return mNameRU;
    }

    public void setNameRU(String nameRU) {
        mNameRU = nameRU;
    }

    public String getRating() {
        return mRating;
    }

    public void setRating(String rating) {
        mRating = rating;
    }

    public String getCountry() {
        return mCountry;
    }

    public void setCountry(String country) {
        mCountry = country;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }

    public String getNameEN() {
        return mNameEN;
    }

    public void setNameEN(String nameEN) {
        mNameEN = nameEN;
    }

    public String getLength() {
        return mLength;
    }

    public void setLength(String length) {
        mLength = length;
    }

    public String getYear() {
        return mYear;
    }

    public void setYear(String year) {
        mYear = year;
    }

    @Override
    public String toString() {
        return mNameRU;
    }

    @Override
    public int compareTo(@NonNull Film another) {
        return this.mRating.compareTo(another.mRating);
    }
}
