package com.example.gulevskiy.kinopoisk;

import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class KinopoiskFetchr {
    private static final String TAG = "KinopoiskFetchr";
    private static final String URL = "http://api.kinopoisk.cf/";
    private static final String RECEIVED_JSON_MESSAGE = "Received JSON: ";
    private static final String FAILED_TO_FETCH_MESSAGE = "Failed to fetch items";
    private static final String FAILED_TO_PARSE_MESSAGE = "Failed to parse JSON";

    public byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() + ": with " + urlSpec);
            }

            int bytesRead;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }

    public String getUrlString(String urlSpec) throws IOException {
        return new String(getUrlBytes(urlSpec));
    }

    public List<Film> fetchFilms(Integer cityId) {
        final String METHOD = "getTodayFilms";
        final String CITY_ID = "cityID";

        List<Film> filmList = new ArrayList<>();

        try {
            String url = Uri.parse(URL + METHOD + "?")
                    .buildUpon()
                    .appendQueryParameter(CITY_ID, Integer.toString(cityId))
                    .build().toString();
            String jsonString = getUrlString(url);
            Log.i(TAG, RECEIVED_JSON_MESSAGE + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseFilmItems(filmList, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, FAILED_TO_FETCH_MESSAGE, ioe);
        } catch (JSONException e) {
            Log.e(TAG, FAILED_TO_PARSE_MESSAGE, e);
        }

        return filmList;
    }

    private void parseFilmItems(List<Film> filmList, JSONObject jsonBody)
            throws JSONException {
        JSONArray jsonArray = jsonBody.getJSONArray("filmsData");

        for (int i = 1; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            Film film = new Film();
            film.setId(jsonObject.getInt("id"));
            film.setNameRU(jsonObject.getString("nameRU"));
            film.setGenre(jsonObject.getString("genre"));
            film.setRating(jsonObject.getString("rating"));
            filmList.add(film);
        }
    }

    public List<Genre> fetchGenres() {
        final String METHOD = "getGenres";

        List<Genre> genreList = new ArrayList<>();

        try {
            String url = Uri.parse(URL + METHOD)
                    .buildUpon()
                    .build().toString();
            String jsonString = getUrlString(url);
            Log.i(TAG, RECEIVED_JSON_MESSAGE + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseGenreItems(genreList, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, FAILED_TO_FETCH_MESSAGE, ioe);
        } catch (JSONException e) {
            Log.e(TAG, FAILED_TO_PARSE_MESSAGE, e);
        }

        return genreList;
    }

    private void parseGenreItems(List<Genre> genreList, JSONObject jsonBody)
            throws JSONException {
        JSONArray jsonArray = jsonBody.getJSONArray("genreData");

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            Genre genre = new Genre();
            genre.setName(jsonObject.getString("genreName"));
            genreList.add(genre);
        }
    }

    public List<Cinema> fetchCinemas(Integer cityId) {
        final String METHOD = "getCinemas";
        final String CITY_ID = "cityID";

        List<Cinema> cinemaList = new ArrayList<>();

        try {
            String url = Uri.parse(URL + METHOD + "?")
                    .buildUpon()
                    .appendQueryParameter(CITY_ID, Integer.toString(cityId))
                    .build().toString();
            String jsonString = getUrlString(url);
            Log.i(TAG, RECEIVED_JSON_MESSAGE + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseCinemaItems(cinemaList, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, FAILED_TO_FETCH_MESSAGE, ioe);
        } catch (JSONException e) {
            Log.e(TAG, FAILED_TO_PARSE_MESSAGE, e);
        }

        return cinemaList;
    }

    private void parseCinemaItems(List<Cinema> cinemaList, JSONObject jsonBody)
            throws JSONException {
        JSONArray jsonArray = jsonBody.getJSONArray("items");

        for (int i = 2; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            Cinema cinema = new Cinema();
            cinema.setName(jsonObject.getString("cinemaName"));
            cinema.setAddress(jsonObject.getString("address"));
            cinema.setLongitude(jsonObject.getDouble("lon"));
            cinema.setLatitude(jsonObject.getDouble("lat"));
            cinemaList.add(cinema);
        }
    }

    public void fetchFilmDetails(Film film) {
        final String METHOD = "getFilm";
        final String FILM_ID = "filmID";

        try {
            String url = Uri.parse(URL + METHOD + "?")
                    .buildUpon()
                    .appendQueryParameter(FILM_ID, Integer.toString(film.getId()))
                    .build().toString();
            String jsonString = getUrlString(url);
            Log.i(TAG, RECEIVED_JSON_MESSAGE + jsonString);
            JSONObject jsonBody = new JSONObject(jsonString);
            parseAndSetFilmDetails(film, jsonBody);
        } catch (IOException ioe) {
            Log.e(TAG, FAILED_TO_FETCH_MESSAGE, ioe);
        } catch (JSONException e) {
            Log.e(TAG, FAILED_TO_PARSE_MESSAGE, e);
        }
    }

    private void parseAndSetFilmDetails(Film film, JSONObject jsonBody)
            throws JSONException {
        film.setDescription(jsonBody.getString("description"));
        film.setNameEN(jsonBody.getString("nameEN"));
        film.setCountry(jsonBody.getString("country"));
        film.setLength(jsonBody.getString("filmLength"));
        film.setYear(jsonBody.getString("year"));
    }
}
